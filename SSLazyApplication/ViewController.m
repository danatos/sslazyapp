//
//  ViewController.m
//  SSLazyApplication
//
//  Created by Sergiy Shevchuk on 1/4/16.
//  Copyright © 2016 Sergiy Shevchuk. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
