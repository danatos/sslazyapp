//
//  main.m
//  SSLazyApplication
//
//  Created by Sergiy Shevchuk on 1/4/16.
//  Copyright © 2016 Sergiy Shevchuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
